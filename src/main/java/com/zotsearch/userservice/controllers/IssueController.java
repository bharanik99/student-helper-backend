package com.zotsearch.userservice.controllers;

import com.zotsearch.userservice.Repositories.ProjectRepo;
import com.zotsearch.userservice.Repositories.IssueRepo;
import com.zotsearch.userservice.Repositories.UserRepo;
import com.zotsearch.userservice.controllers.dto.IssueDto;
import com.zotsearch.userservice.model.Project;
import com.zotsearch.userservice.model.Issue;
import com.zotsearch.userservice.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
public class IssueController {

    @Autowired
    IssueRepo repo;
    @Autowired
    UserRepo userRepo;
    @Autowired
    ProjectRepo projectRepo;


    @RequestMapping("/issues")
    public List<Issue> getIssues() {
        return repo.findAll();
    }

    @RequestMapping("/issues/{id}")
    public Issue getIssueInfo(@PathVariable int id) {

        return repo.findById(id).get();
    }

    @PostMapping("/issues/add")
    @Transactional
    public Issue saveOrUpdateIssue(@RequestBody IssueDto issueDto) {
        Issue issue = null;
        Optional<Issue> present = repo.findById(issueDto.getId());
        User reporter = userRepo.findById(issueDto.getReporterId()).get();
        Project project = projectRepo.findById(issueDto.getProjectId()).get();
        List<User> users = new ArrayList<>();
        for(String user : issueDto.getUserIds())
            users.add(userRepo.findById(user).get());
        if(present.isPresent()){
            issue = present.get();
            issue.setTitle(issueDto.getTitle());
            issue.setType(issueDto.getType());
            issue.setStatus(issueDto.getStatus());
            issue.setPriority(issueDto.getPriority());
            issue.setListPosition(issueDto.getListPosition());
            issue.setDescription(issueDto.getDescription());
            issue.setDescriptionText(issueDto.getDescriptionText());
            issue.setEstimate(issueDto.getEstimate());
            issue.setTimeSpent(issueDto.getTimeSpent());
            issue.setTimeRemaining(issueDto.getTimeRemaining());
            issue.setReporterId(reporter);
            issue.setProject(project);
            issue.setUsers(users);
            issue.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
        }
        else {
            issue = Issue.builder()
                    .title(issueDto.getTitle())
                    .type(issueDto.getType())
                    .status(issueDto.getStatus())
                    .priority(issueDto.getPriority())
                    .listPosition(issueDto.getListPosition())
                    .description(issueDto.getDescription())
                    .descriptionText(issueDto.getDescriptionText())
                    .estimate(issueDto.getEstimate())
                    .timeSpent(issueDto.getTimeSpent())
                    .timeRemaining(issueDto.getTimeRemaining())
                    .reporterId(reporter)
                    .project(project)
                    .users(users)
                    .createdAt(new Timestamp(System.currentTimeMillis()))
                    .updatedAt(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
        return repo.save(issue);
    }
}
