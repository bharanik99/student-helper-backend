package com.zotsearch.userservice.controllers;

import com.zotsearch.userservice.Repositories.ProjectRepo;
import com.zotsearch.userservice.Repositories.IssueRepo;
import com.zotsearch.userservice.Repositories.UserRepo;
import com.zotsearch.userservice.controllers.dto.ProjectDto;
import com.zotsearch.userservice.model.Project;
import com.zotsearch.userservice.model.Issue;
import com.zotsearch.userservice.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
public class ProjectController {

    @Autowired
    ProjectRepo repo;
    @Autowired
    UserRepo userRepo;
    @Autowired
    IssueRepo issueRepo;

    @RequestMapping("/projects")
    public List<Project> getProjects() {
        return repo.findAll();
    }

    @RequestMapping("/projects/{id}")
    public Map<String, Project> getProjectInfo(@PathVariable int id) {
        Project p = repo.findById(id).get();
        Map<String, Project> ans = new HashMap<>();
        ans.put("project", p);
        return ans;
    }

    @PostMapping("/projects/add")
    @Transactional
    public Project saveOrUpdateIssue(@RequestBody ProjectDto projectDto) {
        List<User> users = new ArrayList<>();
        for(String userid : projectDto.getUsers())
            users.add(userRepo.findById(userid).get());
        List<Issue> issues = new ArrayList<>();
        for(Integer issue : projectDto.getIssues())
            issues.add(issueRepo.findById(issue).get());
        Project project = Project.builder()
                .name(projectDto.getName())
                .description(projectDto.getDescription())
                .createdAt(new Timestamp(System.currentTimeMillis()))
                .updatedAt(new Timestamp(System.currentTimeMillis()))
                .users(users)
                .issues(issues)
                .build();
        return repo.save(project);
    }
}
