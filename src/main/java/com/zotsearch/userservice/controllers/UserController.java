package com.zotsearch.userservice.controllers;

import com.zotsearch.userservice.Repositories.UserRepo;
import com.zotsearch.userservice.controllers.dto.UserDto;
import com.zotsearch.userservice.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class UserController {

    @Autowired
    UserRepo repo;


//    @PreAuthorize("hasRole('ROLE_CUSTOMER')")
    @RequestMapping("/users")
    public List<User> getCustomers() {
        return repo.findAll();
    }

    @RequestMapping("/users/{id}")
    public User getUserInfo(@PathVariable String id) {
        return repo.findById(id).get();
    }

    @PostMapping("/users/add")
    @Transactional
    public User saveOrUpdateUser(@RequestBody User user) {
        return repo.save(user);
    }

    @PostMapping("/users/login")
    @Transactional
    public User login(@RequestBody UserDto user) throws Exception {
        if(user.getId().length() == 0) throw new Exception("Empty ID");
        User curr = repo.findById(user.getId()).get();
//        if(curr == null) throw new Exception("User does not exist");
        if(!curr.getPassword().equals(user.getPassword())) throw new Exception("Incorrect Password");
        return curr;
    }
}
