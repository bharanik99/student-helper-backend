package com.zotsearch.userservice.controllers.dto;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProjectDto {
    private String name;
    private String description;
    private List<String> users;
    private List<Integer> issues;

    public ProjectDto(String name, String description, List<String> users) {
        this.name = name;
        this.description = description;
        this.users = users;
        issues = new ArrayList<>();
    }
}
