package com.zotsearch.userservice.controllers.dto;

import com.zotsearch.userservice.model.Comment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class IssueDto {
//    {"type":"task","title":"Bharani task",
//            "description":"","reporterId":"Snehak",
//            "userIds":["bharanik"],"priority":"3",
//            "status":"backlog","projectId":1,"users":[{"id":"bharanik"}]}
    private int id;
    private String title;
    private String type;
    private String status;
    private String priority;
    private int listPosition;

    private String description;
    private String descriptionText;
    private Double estimate;
    private Double timeSpent;
    private Double timeRemaining;

    private String reporterId;

    private int projectId;

    private List<Comment> comments;

    private List<String> userIds;
}
