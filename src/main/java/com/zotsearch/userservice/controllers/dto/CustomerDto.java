package com.zotsearch.userservice.controllers.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CustomerDto {

    private String customerId;
    private int height;
    private float weight;
    private String emailId;
    private String phoneNumber;

}
