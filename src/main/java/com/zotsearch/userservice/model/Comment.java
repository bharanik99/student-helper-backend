package com.zotsearch.userservice.model;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Comment {
	@Id
	@Column(unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String body;
	private Timestamp createdAt;
	private Timestamp updatedAt;

	@ManyToOne
	private User user;

	@ManyToOne
	private Issue issue;
}

/*
"comments": [
			{
				"id": 631285,
				"body": "An old silent pond...\nA frog jumps into the pond,\nsplash! Silence again.",
				"createdAt": "2022-05-15T22:21:38.276Z",
				"updatedAt": "2022-05-15T22:21:38.276Z",
				"userId": 235144,
				"issueId": 633730,
				"user": {
					"id": 235144,
					"name": "Lord Gaben",
					"email": "gaben@jira.guest",
					"avatarUrl": "https://i.ibb.co/6RJ5hq6/gaben.jpg",
					"createdAt": "2022-05-15T22:21:38.212Z",
					"updatedAt": "2022-05-15T22:21:38.226Z",
					"projectId": 78134
				}
			}
		],
* */