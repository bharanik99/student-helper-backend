package com.zotsearch.userservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.stream.Collectors;

@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Issue {
	@Id
	@Column(unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String title;
	private String type;
	private String status;
	private String priority;
	private int listPosition;
	private Timestamp createdAt;
	private Timestamp updatedAt;

	@Lob
	private String description;
	@Lob
	private String descriptionText;
	private Double estimate;
	private Double timeSpent;
	private Double timeRemaining;

	@ManyToOne
	@JoinColumn(name = "reporter_id", nullable = false)
	@JsonIgnoreProperties("projects")
	private User reporterId;


	@ManyToOne
	@JoinColumn(name = "project_id")
	@JsonIgnoreProperties("issues")
	private Project project;

	@OneToMany(mappedBy = "issue")
	@JsonIgnoreProperties("issue")
	private Collection<Comment> comments;

	@ManyToMany
	@JoinTable(
					name = "issue_users",
					joinColumns = { @JoinColumn(name = "issue_id") },
					inverseJoinColumns = { @JoinColumn(name = "user_id") }
	)
	@JsonIgnoreProperties("projects")
	private Collection<User> users;

	@Transient
	private Collection<String> userIds;

	public Collection<String> getUserIds() {
		return users.stream().map(User::getId).collect(Collectors.toList());
	}
}
//      {
//							"id": 633731,
//							"title": "Each issue has a single reporter but can have multiple assignees.",
//							"type": "story",
//							"status": "selected",
//							"priority": "4",
//							"listPosition": 6,
//							"createdAt": "2022-05-15T22:21:38.255Z",
//							"updatedAt": "2022-05-15T22:21:38.255Z",
//							"userIds": [
//							235144,
//							235145
//							]
//							},
/*
{
	"issue": {
		"id": 633730,
		"title": "This is an issue of type: Task.",
		"type": "task",
		"status": "backlog",
		"priority": "4",
		"listPosition": 1,
		"description": "<p>Your teams can collaborate in Jira applications by breaking down pieces of work into issues. Issues can represent tasks, software bugs, feature requests or any other type of project work.</p><p><br></p><h3>Jira Software&nbsp;(software projects) issue types:</h3><p><br></p><h1><strong>Bug </strong><span style=\"background-color: initial;\">🐞</span></h1><p>A bug is a problem which impairs or prevents the functions of a product.</p><p><br></p><h1><strong>Story </strong><span style=\"color: rgb(51, 51, 51);\">📗</span></h1><p>A user story is the smallest unit of work that needs to be done.</p><p><br></p><h1><strong>Task </strong><span style=\"color: rgb(51, 51, 51);\">🗳</span></h1><p>A task represents work that needs to be done.</p>",
		"descriptionText": "Your teams can collaborate in Jira applications by breaking down pieces of work into issues. Issues can represent tasks, software bugs, feature requests or any other type of project work.Jira Software&nbsp;(software projects) issue types:Bug 🐞A bug is a problem which impairs or prevents the functions of a product.Story 📗A user story is the smallest unit of work that needs to be done.Task 🗳A task represents work that needs to be done.",
		"estimate": 8,
		"timeSpent": 4,
		"timeRemaining": null,
		"createdAt": "2022-05-15T22:21:38.239Z",
		"updatedAt": "2022-05-15T22:21:38.239Z",
		"reporterId": 235145,
		"projectId": 78134,
		"users": [
			{
				"id": 235144,
				"name": "Lord Gaben",
				"email": "gaben@jira.guest",
				"avatarUrl": "https://i.ibb.co/6RJ5hq6/gaben.jpg",
				"createdAt": "2022-05-15T22:21:38.212Z",
				"updatedAt": "2022-05-15T22:21:38.226Z",
				"projectId": 78134
			}
		],
		"comments": [
			{
				"id": 631285,
				"body": "An old silent pond...\nA frog jumps into the pond,\nsplash! Silence again.",
				"createdAt": "2022-05-15T22:21:38.276Z",
				"updatedAt": "2022-05-15T22:21:38.276Z",
				"userId": 235144,
				"issueId": 633730,
				"user": {
					"id": 235144,
					"name": "Lord Gaben",
					"email": "gaben@jira.guest",
					"avatarUrl": "https://i.ibb.co/6RJ5hq6/gaben.jpg",
					"createdAt": "2022-05-15T22:21:38.212Z",
					"updatedAt": "2022-05-15T22:21:38.226Z",
					"projectId": 78134
				}
			}
		],
		"userIds": [
			235144
		]
	}
}
* */