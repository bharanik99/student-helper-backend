package com.zotsearch.userservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Project {

	@Id
	@Column(unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
	private String description;
	private Timestamp createdAt;
	private Timestamp updatedAt;

	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(
					name = "project_users",
					joinColumns = { @JoinColumn(name = "project_id") },
					inverseJoinColumns = { @JoinColumn(name = "user_id") }
	)
	@JsonIgnoreProperties("projects")
	private Collection<User> users;

	@OneToMany(mappedBy = "project")
	@JsonIgnoreProperties("project")
	private Collection<Issue> issues;

	@Override
	public String toString() {
		return "Project{" +
						"id=" + id +
						", name='" + name + '\'' +
						", description='" + description + '\'' +
						", createdAt=" + createdAt +
						", updatedAt=" + updatedAt +
						", users=" + users +
						", issues=" + issues +
						'}';
	}
}

/*
{
  "project": {
    "id": 78134,
    "name": "singularity 1.0",
    "url": "https://www.atlassian.com/software/jira",
    "description": "Plan, track, and manage your agile and software development projects in Jira. Customize your workflow, collaborate, and release great software.",
    "category": "software",
    "createdAt": "2022-05-15T22:21:38.226Z",
    "updatedAt": "2022-05-15T22:21:38.226Z",
    "users": [
      {
        "id": 235146,
        "name": "Pickle Rick",
        "email": "rick@jira.guest",
        "avatarUrl": "https://i.ibb.co/7JM1P2r/picke-rick.jpg",
        "createdAt": "2022-05-15T22:21:38.215Z",
        "updatedAt": "2022-05-15T22:21:38.226Z",
        "projectId": 78134
      },
      {
        "id": 235145,
        "name": "Baby Yoda",
        "email": "yoda@jira.guest",
        "avatarUrl": "https://i.ibb.co/6n0hLML/baby-yoda.jpg",
        "createdAt": "2022-05-15T22:21:38.214Z",
        "updatedAt": "2022-05-15T22:21:38.226Z",
        "projectId": 78134
      },
      {
        "id": 235144,
        "name": "Lord Gaben",
        "email": "gaben@jira.guest",
        "avatarUrl": "https://i.ibb.co/6RJ5hq6/gaben.jpg",
        "createdAt": "2022-05-15T22:21:38.212Z",
        "updatedAt": "2022-05-15T22:21:38.226Z",
        "projectId": 78134
      }
    ],
    "issues": [
      {
        "id": 633728,
        "title": "Try dragging issues to different columns to transition their status.",
        "type": "story",
        "status": "backlog",
        "priority": "3",
        "listPosition": 3,
        "createdAt": "2022-05-15T22:21:38.237Z",
        "updatedAt": "2022-05-15T22:21:38.237Z",
        "userIds": []
      },
      {
        "id": 633729,
        "title": "Click on an issue to see what's behind it.",
        "type": "task",
        "status": "backlog",
        "priority": "2",
        "listPosition": 2,
        "createdAt": "2022-05-15T22:21:38.238Z",
        "updatedAt": "2022-05-15T22:21:38.238Z",
        "userIds": [
          235146
        ]
      },
      {
        "id": 633730,
        "title": "This is an issue of type: Task.",
        "type": "task",
        "status": "backlog",
        "priority": "4",
        "listPosition": 1,
        "createdAt": "2022-05-15T22:21:38.239Z",
        "updatedAt": "2022-05-15T22:21:38.239Z",
        "userIds": [
          235146
        ]
      },
      {
        "id": 633731,
        "title": "Each issue has a single reporter but can have multiple assignees.",
        "type": "story",
        "status": "selected",
        "priority": "4",
        "listPosition": 6,
        "createdAt": "2022-05-15T22:21:38.255Z",
        "updatedAt": "2022-05-15T22:21:38.255Z",
        "userIds": [
          235144,
          235145
        ]
      },
      {
        "id": 633732,
        "title": "Try leaving a comment on this issue.",
        "type": "task",
        "status": "done",
        "priority": "3",
        "listPosition": 7,
        "createdAt": "2022-05-15T22:21:38.256Z",
        "updatedAt": "2022-05-15T22:21:38.256Z",
        "userIds": [
          235145
        ]
      },
      {
        "id": 633733,
        "title": "Each issue can be assigned priority from lowest to highest.",
        "type": "task",
        "status": "selected",
        "priority": "5",
        "listPosition": 5,
        "createdAt": "2022-05-15T22:21:38.258Z",
        "updatedAt": "2022-05-15T22:21:38.258Z",
        "userIds": []
      },
      {
        "id": 633735,
        "title": "You can track how many hours were spent working on an issue, and how many hours remain.",
        "type": "task",
        "status": "inprogress",
        "priority": "1",
        "listPosition": 7,
        "createdAt": "2022-05-15T22:21:38.260Z",
        "updatedAt": "2022-05-15T22:21:38.260Z",
        "userIds": []
      },
      {
        "id": 633734,
        "title": "You can use rich text with images in issue descriptions.",
        "type": "story",
        "status": "backlog",
        "priority": "1",
        "listPosition": 4,
        "createdAt": "2022-05-15T22:21:38.263Z",
        "updatedAt": "2022-05-15T22:21:38.263Z",
        "userIds": [
          235144
        ]
      }
    ]
  }
}
*/
