package com.zotsearch.userservice.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class User {
	@Id
	@Column(unique = true, nullable = false)
	private String id;
	@JsonProperty("name")
	private String firstName;
	private String lastName;
	private String email;
	private String avatar;
	private String phone;
	private String role;
	@JsonIgnore
	private String password;

	@ManyToMany(mappedBy = "users")
	@JsonIgnoreProperties({"users","issues"})
	private Collection<Project> projects;

	@Override
	public String toString() {
		return "User{" +
						"id='" + id + '\'' +
						", firstName='" + firstName + '\'' +
						", lastName='" + lastName + '\'' +
						", email='" + email + '\'' +
						", avatar='" + avatar + '\'' +
						", phone='" + phone + '\'' +
						", role='" + role + '\'' +
						", password='" + password + '\'' +
						", projects=" + projects.stream().mapToInt(Project::getId) +
						'}';
	}
}
