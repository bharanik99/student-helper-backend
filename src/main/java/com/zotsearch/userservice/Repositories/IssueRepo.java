package com.zotsearch.userservice.Repositories;

import com.zotsearch.userservice.model.Issue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource()
public interface IssueRepo extends JpaRepository<Issue, Integer>{
//	@Query(value= "select ticket_id, creator_id, assignee_id, title, description, summary, story_points, created_time" +
//					" from ticket" +
//					" where ticket_id = ?1",
//    		nativeQuery = true)
//	public Map<String, Object> getTicketInfo(int ticketId);
}


