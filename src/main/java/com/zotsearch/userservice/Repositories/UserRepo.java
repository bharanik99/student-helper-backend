package com.zotsearch.userservice.Repositories;

import com.zotsearch.userservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;
import java.util.Map;

@RepositoryRestResource()
public interface UserRepo extends JpaRepository<User, String>{

//	@PreAuthorize("hasRole('ROLE_CUSTOMER')")
//	@Query(value= "select id, first_name, last_name, email_id, phone, `role`, auth_token, `password` from user where id = ?1",
//    		nativeQuery = true)
//	User getUserInfo(String userId);

}


