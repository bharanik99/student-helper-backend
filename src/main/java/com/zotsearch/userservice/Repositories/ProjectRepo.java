package com.zotsearch.userservice.Repositories;

import com.zotsearch.userservice.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource()
public interface ProjectRepo extends JpaRepository<Project, Integer>{
}


